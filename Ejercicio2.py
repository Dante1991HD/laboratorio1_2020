#!/usr/bin/python
# -*- coding: utf-8 -*-


import random
# Toma matriz generada para dar formato elegante a la salida


def impresora(matriz):
    for i in range(len(matriz)):
        for j in range(len(matriz[i])):
            print('|_{0:^3}_|'.format(matriz[i][j]), end=' ')
        print()


# Genera una lista con la suma de las 5 columnas
# y genera una salida con la información


def suma_columnas(matriz):
    suma = []
    for i in range(5):
        temp_suma = 0
        for j in range(15):
            temp_suma += matriz[j][i]
        suma.append(temp_suma)
    print("La suma de las primeras 5 columnas es:", suma)


# utiliza el primer valor de la matriz como pivote,
# este se compara con todos y se sobre escribe al encontrar uno menor,
# finalmente imprime el menos valor encontrado en la matriz


def menor_elemento(matriz):
    menor = matriz[0][0]
    for i in range(15):
        for j in range(12):
            if menor > matriz[i][j]:
                menor = matriz[i][j]
    print("El numero menor en la matriz creada aleatoriamente es:", menor)


# por medio de una condicion (valor menor a 0) contabiliza el
# numero de coincidencias en base a esa condicion
# luego la muestr en pantalla


def elementos_negativos(matriz):
    total_negativos = 0
    for i in range(15):
        for j in range(5, 9):
            if matriz[i][j] < 0:
                total_negativos += 1
    print("total negativos de columna 5 a 9:", total_negativos)


# Main que crea la matriz y subsecuentemente llama a las funciones
if __name__ == "__main__":
   fila_columna = (12, 15)
    matriz = [[random.randint(-10, 10) for i in range(fila_columna[0])] for i in range(fila_columna[1])]
    impresora(matriz)
    suma_columnas(matriz)
    menor_elemento(matriz)
    elementos_negativos(matriz)
