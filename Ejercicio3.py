#!/usr/bin/python
# -*- coding: utf-8 -*-


import random


# Compara el promedio con el requisito de aprobación para establecer
# si el estudiante aprueba o reprueba
# ademas contabiliza los que aprobaron, los que reprueban se calculan
# restando los que aprobaron del total


def establece_promocion(promedio):
    requisito = (4)
    pasan = 0
    for x in promedio:
        if x[1] < requisito:
            print('El estudiante {0} reprueba con un {1}'.format(x[0], x[1]))
            pasan += 1
        else:
            print('El estudiante {0} aprueba con un {1}'.format(x[0], x[1]))
    print("Aprobados {} reprobados {}".format(pasan, len(promedio) - pasan))


# se crea una lista por comprensión para la suma,
# esta ligada a el codigo del estudiante,
# se llama a "establece_promocion" para verificar las condiciones de aprobación


def calcula_promedios(estudiantes):
    promedio = [[x+1, round(sum(estudiantes[x][1])/4, 1)] for x in range(31)]
    establece_promocion(promedio)


# se crea una lista con los alumnos y sus notas de manera aleatoria,
# se deja como regla un decimal, las notas son de tipo float,
# se establece 4 notas por un sentido anecdotico
# (el mínimo en la utal para promedios)


estudiantes = [[x+1, [round(random.uniform(1, 7), 1) for j in range(4)]]
                      for x in range(31)]
calcula_promedios(estudiantes)
