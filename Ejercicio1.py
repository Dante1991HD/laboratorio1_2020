#!/usr/bin/python
# -*- coding: utf-8 -*-

# Tupla a transformar a lista con las condiciones pedidas por el ejercicio


tupla = (1, 2, 3, 4, 5)

# se crea una lista por comprensión con las condiciones del ejercicio,
# si es el primer elemento se le ubica el ultimo,
# todos los demas siguen el orden correlativo desplazandose una posición


def modifica_posicion(tupla):
    print("Tupla a pasar a lista y cambiar orden", tupla)
    lista = [tupla[len(tupla)-1] if i == 0
             else tupla[i-1] for i in range(len(tupla))]
    print("Con el cambio de orden", lista)
modifica_posicion(tupla)
